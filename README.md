# KAJ – OneMinuteChanges

Semestralní projekt pro předmět KAJ – LS 2020/2021

**Cíl:**
Vytvoření aplikace, která by sloužila jako pomocník k tréninku hraní na kytaru. Konkrétně pak k cvičící technice One Minute Changes viz [YouTube](https://www.youtube.com/watch?v=mAgc7hr44WM&t=2s&ab_channel=JustinGuitar).

**Popis funkčnosti:**
Uživatel si přidá žádanou dvojici akordů k procvičování. U tohoto mini-formuláře je i validace na prázdnost políček a zda daná dvojice už neexistuje, aby nevznikaly duplicity.
Po úspěšném přidání tréninku může uživatel daný trénink trvale smazat, zobrazit si historii tréninků a skóre a pak samozřejmě spustit samotný trénink.
V rámci tréninku je implementován minutový časovač, který i uživatele zvukově upozorní na začátek i konec tréninkové doby. Po skončení tréninku uživatel může zapsat svoje skóre a výsledek uložit.
Tyto výsledky jsou pak u daného tréninku trvalu uloženy (v local storage) a uživetel si je může zobrazit pomocí rozbalovací šipky pod tréninkem. Tento postup se zobrazuje pomocí grafu.

V pravé části obrazovky se nachází tlačítko s "i", pod kterým je skrytý jednoduchý popis využívání aplikace a odkaz na tutoriál k One Minute Changes.

Tlačítko v levé dolní části umožňuje stlumit zvukové efekty při tréninku.
Tlačítko v pravé dolní části umožňuje jednoduché přesunutí se na začátek stránky. Toto talčítko se objeví jen, když v rámci stránky sjedete trochu níže.

Aplikace je responsivní – určena pro mobilní zařízení, ale i počítač.

Vytvořeno pomocí **HTML** a **CSS** (bez preprocesorů), hlavní logika aplikace je **v čistém JavaScriptu**. V JavaScriptové části je využita knihovna **jQuery** v. 3.2.1, pro vykreslování grafů pak knihovna **Chart.js** Jako vývojové prostředí jsem používal VS Code.
