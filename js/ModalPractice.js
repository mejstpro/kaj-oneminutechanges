class ModalPractice {
	/**
	 * Creates modal window of the practice
	 * @param {OneMinuteChange} change which is practiced
	 */
	constructor(change) {
		this.change = change;
		/**@type {Array} */
		this.intervals = [];
		/**@type {Audio} */
		this.currentAudio = null;
		/**@type {Array} */
		this.elToDisable = [];
		this.startDelay = 3;
		this.practiceTime = 60;
		this.urlStartAudio = "./audio/startPractice.mp3";
		this.urlEndAudio = "./audio/endPracticeE.mp3";
	}

	/**
	 * Starts the practice.
	 * @returns {Promise} after practice has ended. Resolve is the score entered (type of number), reject when modal closed without saving.
	 */
	practice() {
		return new Promise((resolve, reject) => {
			let closeBtn = createBtn(
				"btn-close",
				'<i class="fas fa-times"></i>',
				() => {
					this._close();
					reject("Closed by cross.");
				}
			);
			let headingEl = $(
				`<h3>${this.change.chord1}<span class="dash"> – </span>${this.change.chord2}</h3>`
			);
			let clock = $('<div id="clock"></div>');
			this._drawClock(clock);

			// Score
			let scoreForm = $(`
				<form id="score">
					<label for="score">Score:</label>
					<input type="number" id="input-score" name="score"/>
				</form>
			`);

			// Buttons
			let btnsEl = $(`<div class="btns"></div>`);
			let btnSave = createBtn("btn-save", "Save", (e) => {
				e.preventDefault();
				this._close();
				resolve($("#input-score").val());
			});
			btnSave.attr("type", "submit");
			// set focus to yes (for pressing Enter)
			btnSave.trigger("focus");

			let btnDiscard = createBtn("btn-discard", "Discard", (e) => {
				e.preventDefault();
				this._close();
				reject("Closed by discard.");
			});

			btnsEl.append(btnSave, btnDiscard);
			scoreForm.append(btnsEl);

			// disable score elements
			scoreForm.attr("disabled", true);
			$(scoreForm)
				.find("*")
				.toArray()
				.forEach((element) => $(element).attr("disabled", true));

			// append elements
			let modalEl = $('<div id="modal-practice"></div>');
			let practiceEl = $('<div class="card-practice"></div>');
			practiceEl.append(closeBtn, headingEl, clock, scoreForm);
			modalEl.append(practiceEl);
			$("#modals").append(modalEl).hide().fadeIn(200);

			// prevent scrolling when modal is shown
			$("body").addClass("preventScroll");

			this._startPractice();
		});
	}

	/**
	 * Close modal
	 */
	_close() {
		$("#modal-practice").fadeOut(200, function () {
			$(this).remove();
		});
		$("body").removeClass("preventScroll");

		// stop all intervals
		this.intervals.forEach((interval) => {
			clearInterval(interval);
		});
		this.currentAudio.pause();
		this.currentAudio.currentTime = 0;
	}

	/**
	 * Draws SVG clock into given element.
	 * @param {JQuery} toElement into which element should be the clock drawn
	 */
	_drawClock(toElement) {
		let svg = $('<svg width="200" height="200"></svg>');
		for (let i = 0; i <= 11; i++) {
			let line = $(
				document.createElementNS("http://www.w3.org/2000/svg", "line")
			);
			line.attr("x1", "100");
			line.attr("y1", "30");
			line.attr("x2", "100");
			line.attr("y2", "40");
			line.attr("transform", `rotate( ${(i * 360) / 12} 100 100)`);
			svg.append(line);
		}
		let counterText = $('<p class="counter"></p>');
		counterText.text(this.practiceTime);
		toElement.append(svg, counterText);
	}

	/**
	 * Starst the practice countdown.
	 */
	_startPractice() {
		console.log("Pracice started.");
		let counterText = $(".counter");
		let lines = $("svg line");

		counterText.text(this.startDelay);
		this._setAllLinesColor(lines, "orange");
		this._startSound();

		this._startCounter(this.startDelay, counterText)
			.then(() => {
				counterText.text("Go!");
				this._setAllLinesColor(lines, "red");
				this._startRemovingRedLines(lines, this.practiceTime);
				return this._startCounter(this.practiceTime, counterText);
			})
			.then(() => {
				counterText.text(0);
				setTimeout(() => {
					counterText.text("Yes!");
					this._setAllLinesColor(lines, "green");
					this._endSound();

					//enable score elements
					$("form#score").attr("disabled", false);
					$("form#score *")
						.toArray()
						.forEach((element) => $(element).attr("disabled", false));
					$("#input-score").trigger("focus");
				}, 1000);
			});
	}

	/**
	 *
	 * @param {number} time number of secconds to count
	 * @param {JQuery} counterText element into which should be the time rendered
	 * @returns {Promise} after the counter ends. Type of resolve is boolean
	 */
	_startCounter(time, counterText) {
		return new Promise((resolve) => {
			let startCountdown = setInterval(() => {
				time--;
				if (time > 0) {
					counterText.text(time);
				} else {
					clearInterval(startCountdown);
					resolve(true);
				}
			}, 1000);
			this.intervals.push(startCountdown);
		});
	}

	/**
	 * Adds given css class to all of the given elements.
	 * @param {JQuery[]} lines to which set the css class
	 * @param {string} color name of the css class
	 */
	_setAllLinesColor(lines, color) {
		lines.toArray().forEach((line) => {
			$(line).removeClass();
			$(line).addClass(color);
		});
	}

	/**
	 * Sets timer for removing the CSS red class of the SVG elements
	 * @param {JQuery[]} lines on which lines remove the red CSS class
	 * @param {number} time how long is the practice (in seconds)
	 */
	_startRemovingRedLines(lines, time) {
		let interval = time / 12;
		let iteration = 12;
		lines = lines.toArray();
		let startCountdown = setInterval(() => {
			iteration--;
			$(lines[iteration]).removeClass("red");
			if (iteration < 0) {
				clearInterval(startCountdown);
			}
		}, interval * 1000);
		this.intervals.push(startCountdown);
	}

	/**
	 * Play start sound
	 */
	_startSound() {
		if (mute) return;
		this.currentAudio = new Audio(this.urlStartAudio);
		this.currentAudio.play();
	}

	/**
	 * Play end sound
	 */
	_endSound() {
		if (mute) return;
		this.currentAudio = new Audio(this.urlEndAudio);
		this.currentAudio.play();
	}
}
