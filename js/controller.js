const model = new Model($("#oneMinuteChanges"));

/**@type {Array[string]} */
const cheerSentences = [
	"Wow, new Eric Clapton?",
	"Go like a pro!",
	"You want to catch up with Django Reinhardt? Big goals, big results!",
	"Yeah, come on, master it.",
	"Don't stop now.",
];

// Add new change form handler
$("form").on("submit", function (e) {
	e.preventDefault();

	// get elements
	const cheerUpText = $("p.cheer-up");
	const chord1Input = $("#chord1");
	const chord2Input = $("#chord2");
	const chord1 = chord1Input.val();
	const chord2 = chord2Input.val();

	chord1Input.removeClass("red");
	chord2Input.removeClass("red");
	$("#new-change").removeClass("cheer");
	$("#new-change").offset(); //force rerender

	// form validation
	if (!chord1 && !chord2) {
		chord1Input.addClass("red");
		chord2Input.addClass("red");
		cheerUpText.text("Write some pair of chords first!");
		return;
	}
	if (!chord1) {
		chord1Input.addClass("red");
		cheerUpText.text("Not so fast, add the second chord!");
		return;
	}
	if (!chord2) {
		chord2Input.addClass("red");
		cheerUpText.text("Not so fast, add the second chord!");
		return;
	}
	if (_validateSameChange(chord1, chord2)) {
		chord1Input.addClass("red");
		chord2Input.addClass("red");
		cheerUpText.text("You are already training this pair!");
		return;
	}

	// if validation OK
	$("#new-change").addClass("cheer");
	cheerUpText.text(
		cheerSentences[Math.floor(Math.random() * cheerSentences.length)]
	);
	chord1Input.val("");
	chord2Input.val("");

	const newChage = new OneMinuteChange(
		sanitizeHTML(chord1),
		sanitizeHTML(chord2)
	);
	model.addNewChange(newChage);
});

function sanitizeHTML(text) {
	return $("<div>").text(text).html();
}

/**
 * Validates if the same pair of chords is not already in the array of changes
 * @param {string} chord1
 * @param {string} chord2
 * @returns {boolean} if this pair of chords is already in the array of changes
 */
function _validateSameChange(chord1, chord2) {
	let sameChange = model.getChanges().filter((change) => {
		return (
			(change.chord1 === chord1 && change.chord2 === chord2) ||
			(change.chord1 === chord2 && change.chord2 === chord1)
		);
	});
	return sameChange.length === 0 ? false : true;
}

// Aside info panel showing
$("aside").on("click", function () {
	$("aside").toggleClass("show");
});

// Arrow to top
const arrowToTop = $(".⬆️");
$(window).on("scroll", function () {
	if ($(window).scrollTop() > 100) {
		arrowToTop.addClass("show");
	} else {
		arrowToTop.removeClass("show");
	}
});
arrowToTop.on("click", function (e) {
	$("html").animate({ scrollTop: 0 }, "300");
});

// Mute btn
const muteBtn = $(".🔇");
muteBtn.on("click", function () {
	if (mute) {
		muteBtn.html('<i class="fas fa-volume-up"></i>');
		mute = false;
	} else {
		muteBtn.html('<i class="fas fa-volume-mute"></i>');
		mute = true;
	}
});
