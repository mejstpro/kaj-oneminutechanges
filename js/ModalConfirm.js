/**
 * Modal component for confirming user actions.
 */
class ModalConfirm {
	/**
	 * Creates modal with a confirm dialog.
	 * @param {string} text of the modal which is shown to the user.
	 */
	constructor(text) {
		let modalEl = $('<div id="modal-confirm"></div>');
		let cardEl = $('<div class="card"></div>');
		let description = $(`<p>${text}</p>`);
		let btnYes = createBtn("yes", "Yes", () => {});
		let btnNo = createBtn("no", "No", () => {});
		cardEl.append(description, btnYes, btnNo);
		modalEl.append(cardEl);
		$("#modals").append(modalEl).hide().fadeIn(200);

		// prevent scrolling when modal
		$("body").addClass("preventScroll");

		// set focus to yes (for pressing Enter)
		btnYes.trigger("focus");
	}

	/**
	 * Gets result if user clicked Yes or No
	 * @returns {Promise} after user has choosen the answer. Type of resolve is Boolean (true = Yes clicked, false = No clicked)
	 */
	confirm() {
		return new Promise((resolve, reject) => {
			$("#modal-confirm button.yes").on("click", () => {
				resolve(true);
				this._close();
			});
			$("#modal-confirm button.no").on("click", () => {
				resolve(false);
				this._close();
			});
		});
	}

	_close() {
		$("#modal-confirm").fadeOut(200, function () {
			$(this).remove();
		});
		$("body").removeClass("preventScroll");
	}
}
