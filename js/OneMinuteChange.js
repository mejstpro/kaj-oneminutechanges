/**
 * Object holding the state and data of the One Minute Change.
 */
class OneMinuteChange {
	/**
	 * Object holding the state and data of the One Minute Change
	 * @param {string} chord1
	 * @param {string} chord2
	 * @param {Date} date of creation
	 * @param {Record[]} records of practices
	 * @param {boolean} showDetails – if the details should be shown or not
	 */
	constructor(
		chord1,
		chord2,
		date = new Date(),
		records = [],
		showDetails = false
	) {
		this.chord1 = chord1;
		this.chord2 = chord2;
		this.date = date;
		/** @type {Record[]} */
		this.records = records;
		/** @type {boolean} */
		this.showDetails = showDetails;
	}

	/**
	 * Adds given practice record to this chage.
	 * @param {Record} record to add
	 */
	addRecord(record) {
		this.records.push(record);
	}

	/**
	 * Gets the best score of all of the practices
	 * @returns {number} the best score on this change
	 */
	getBestScore() {
		if (this.records.length == 0) {
			return 0;
		}
		let scores = this.records.map((record) => record.score);
		return Math.round(Math.max(...scores));
	}
}
