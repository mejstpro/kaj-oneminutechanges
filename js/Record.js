/**
 * Holds info about one practice
 */
class Record {
	/**
	 * Holds info about one practice
	 * @param {number} score of the practice
	 * @param {Date} date of the practice
	 */
	constructor(score, date = new Date()) {
		this.date = date;
		this.score = score;
	}
}
