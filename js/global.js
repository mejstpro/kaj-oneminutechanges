/**
 * If all sound should be muted.
 * @type {boolean}
 */
let mute = false;

/**
 * Global helper funcion for creating buttons
 * @param {string} cssClass css class
 * @param {string} content text inside the button
 * @param {function} callback function called on click
 * @returns {JQuery} Button element with given content and callback on click.
 */
function createBtn(cssClass, content, callback) {
	const btn = $(`<button class="${cssClass}">${content}</button>`);
	btn.on("click", callback);
	return btn;
}
