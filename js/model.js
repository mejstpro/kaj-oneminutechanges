/**
 * Loads and holds the state of the application and handles the main functionality with the OneMinuteDChanges objects.
 */
class Model {
	/**
	 * @param {JQuery} printEl element into which shoud be the OneMinutChanges printed
	 */
	constructor(printEl) {
		/**@type {OneMinuteChange[]} */
		this.changes = [];
		this._loadChanges();

		/**@type {JQuery} */
		this.printEl = printEl;

		this.printChanges();
	}

	/**
	 * Saves a new Change and updates the view.
	 * @param {OneMinuteChange} oneMinuteChange the new one to save
	 */
	addNewChange(oneMinuteChange) {
		this.changes.push(oneMinuteChange);
		this.saveChanges();
		this.printChanges();
	}

	/**
	 * sorts current changes by date descending
	 */
	_sortChanges() {
		this.changes.sort((change1, change2) => {
			return new Date(change2.date) - new Date(change1.date);
		});
	}

	/**
	 * Saves the array of oneMinuteChanges into the localStorage
	 */
	saveChanges() {
		console.log("Saving changes");
		localStorage.setItem("oneMinuteChanges", JSON.stringify(this.changes));
	}

	/**
	 * Gets all current and saved changes
	 * @returns {OneMinuteChange[]} all current and saved changes
	 */
	getChanges() {
		return this.changes;
	}

	/**
	 * Sets the array of OneMinuteChage objects to the stored ones in the localStorage
	 */
	_loadChanges(withDetails = false) {
		const loadedChanges = localStorage.getItem("oneMinuteChanges");
		if (loadedChanges) {
			const parsedChanges = JSON.parse(loadedChanges);
			parsedChanges.forEach((element) => {
				let change = null;
				if (withDetails) {
					change = new OneMinuteChange(
						element.chord1,
						element.chord2,
						element.date,
						element.records,
						element.showDetails
					);
				} else {
					change = new OneMinuteChange(
						element.chord1,
						element.chord2,
						element.date,
						element.records
					);
				}
				this.changes.push(change);
			});
		}
	}

	/**
	 * Writes the saved One Minute Changes into the HTML page
	 */
	printChanges() {
		if (this.changes.length == 0) {
			this.printEl.html('<p class="nothing">Let&apos;s start!</p>');
			return;
		} else {
			this.printEl.empty();
		}

		this._sortChanges();

		this.changes.forEach((change) => {
			let changeEl = $('<div class="change"></div>');

			let chordsEl = $('<div class="chords"></div>');
			chordsEl.append(
				`${change.chord1}<span class="dash"> – </span>${change.chord2}`
			);

			let bestEl = $('<div class="best"></div>');
			bestEl.append(`<span>Best:</span><br/>${change.getBestScore()}`);

			let btnsEl = $('<div class="btns"></div>');

			btnsEl.append(
				this._createBtnDelete(change),
				this._createBtnPractice(change)
			);

			if (window.innerWidth < 756) {
				chordsEl.append(bestEl);
				changeEl.append(chordsEl);
			} else {
				changeEl.append(chordsEl, bestEl);
			}

			// details
			let detailsEl = this._createElChangeDetails(change);
			let showBtn = createBtn(
				"btn-show",
				`<i class="fas fa-chevron-down"></i>`,
				() => {
					detailsEl.toggle(500, "swing");
					$(showBtn).find("i").toggleClass("rotate");
					change.showDetails = !change.showDetails;
					this.saveChanges();
				}
			);
			changeEl.append(btnsEl, detailsEl, showBtn);
			if (!change.showDetails) {
				detailsEl.hide();
			} else {
				$(showBtn).find("i").toggleClass("rotate");
			}

			this.printEl.append(changeEl);

			if (change.records.length != 0) {
				this._drawGraph(change, $("canvas").last());
			}
		});
	}

	/**
	 * Creates details element for given change
	 * @param {OneMinuteChange} change for which are the details
	 * @returns {JQuery} prepared details element
	 */
	_createElChangeDetails(change) {
		let detailsEl = $('<div class="details"></div>');
		let totalCountEl = $('<p class="total-count"></p>');
		detailsEl.append("<h3>Stats</h3>");
		if (change.records.length != 0) {
			const canvas = $('<div class="graph"><canvas></canvas></div>');
			totalCountEl.text(`Trainings count: ${change.records.length}`);
			detailsEl.append(canvas, totalCountEl);
		} else {
			totalCountEl.text(`No traings done so far. Go ahead!`);
			detailsEl.append(totalCountEl);
		}

		return detailsEl;
	}
	/**
	 * Draws the graph of the practice history.
	 */
	_drawGraph(change, canvas) {
		let labels = [];
		let dataPoints = [];
		change.records.forEach((record) => {
			labels.push(this._parseDateToGraph(record.date));
			dataPoints.push(record.score);
		});
		const data = {
			labels: labels,
			datasets: [
				{
					label: "Score",
					data: dataPoints,
					backgroundColor: "rgba(255, 99, 132, 1)", //color of the dots inside
					borderColor: "rgba(255, 99, 132, 0.8)", //color of the line
					// borderWidth: 3,
					cubicInterpolationMode: "monotone",
					tension: 0.4,
				},
			],
		};
		const options = {
			responsive: true,
			scales: {
				y: {
					suggestedMin: 10,
					suggestedMax: 60,
				},
			},
			plugins: {
				legend: {
					display: false,
				},
			},
		};

		const config = {
			type: "line",
			data: data,
			options: options,
		};
		new Chart(canvas, config);
	}
	/**
	 * Creates practice button for given Change.
	 * @param {OneMinuteChange} change to which create the practice button
	 * @returns {JQuery} created button
	 */
	_createBtnPractice(change) {
		return createBtn("btn-practice", "Practice", () => {
			const practiceModal = new ModalPractice(change);
			practiceModal
				.practice()
				.then((res) => {
					change.addRecord(new Record(res));
					this.saveChanges();
					const scroll = $(window).scrollTop();
					this.printChanges();
					$(window).scrollTop(scroll);
				})
				.catch((err) => console.log(`Practice not finished. ${err}`));
		});
	}
	/**
	 * Creates delete button for given Change.
	 * @param {OneMinuteChange} change to which create the delete button
	 * @returns {JQuery} created button
	 */
	_createBtnDelete(change) {
		let confirmText =
			"Are you sure you want to delete this One minute change? All your progress will be lost.";
		return createBtn("btn-delete", "Delete", () => {
			let modalConfirm = new ModalConfirm(confirmText);
			modalConfirm.confirm().then((res) => {
				if (res === true) {
					this.changes = this.changes.filter((c) => c != change);
					this.saveChanges();
					this.printChanges();
				}
			});
		});
	}

	/**
	 * Parses given date to local format string
	 * @param {Date} date to parse
	 * @returns {string} parsed date to local date format
	 */
	_parseDateToGraph(date) {
		let options = {
			day: "numeric",
			month: "numeric",
			year: "numeric",
		};
		return new Date(date).toLocaleDateString(undefined, options);
	}
}
